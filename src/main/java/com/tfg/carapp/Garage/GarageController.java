package com.tfg.carapp.Garage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.management.OperationsException;

@Controller
public class GarageController {
		
	@Autowired
	GarageService garageService;

	@PostMapping("createcar")
	@ResponseBody
	public Boolean create() throws OperationsException {
		return garageService.create(null, null);
	}
}
