package com.tfg.carapp.Garage;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface GarageRepository extends JpaRepository<Garage, Long>{
			
	Garage findOneByName(String name);
	
	@Query("select count(c) > 0 from Concesionario c where c.name = :name")
		boolean exists(@Param("name") String name);

}
