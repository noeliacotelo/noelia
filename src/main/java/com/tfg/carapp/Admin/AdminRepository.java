package com.tfg.carapp.Admin;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface AdminRepository extends JpaRepository<Admin, Long>{
	
Admin findOneByName(String name);
	

	@Query("select count(c) > 0 from Admin c where c.name = :name")
		boolean exists(@Param("name") String name);

}
