package com.tfg.carapp.Admin;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "Admin")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class,
		  property = "adminId")
public class Admin implements java.io.Serializable {
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

private Long adminId;

private String name;

private Long DNI;

public Long getAdminId() {
	return adminId;
}

public void setAdminId(Long adminId) {
	this.adminId = adminId;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public Long getDNI() {
	return DNI;
}

public void setDNI(Long DNI) {
	this.DNI = DNI;
}

}

