package com.tfg.carapp.Admin;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tfg.carapp.Car.CarService;

public class AdminController {
	
	@Autowired
	AdminService adminService;

	@PostMapping("createadmin")
	@ResponseBody
	public Boolean create() throws OperationsException {
		return adminService.create(null, null);
	}
}
