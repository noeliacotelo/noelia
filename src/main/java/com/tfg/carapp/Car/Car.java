package com.tfg.carapp.Car;

import java.util.List;
import java.util.Set;

import javax.persistence.*;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.tfg.carapp.Repair.Repair;
import com.tfg.carapp.UserAccount.Account;


@Entity
@Table(name = "Repair")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class,
		  property = "carId")
public class Car implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "carId")
private Long carId;

private String name;

private String description;

@ManyToMany
@JoinTable(name = "CarAccount", joinColumns = { @JoinColumn(name = "accountId") }, inverseJoinColumns = {
		@JoinColumn(name = "accountId") })
private List<Account> accounts;

@ManyToMany
@JoinTable(name = "CarAccount", joinColumns = { @JoinColumn(name = "carId") }, inverseJoinColumns = {
		@JoinColumn(name = "carId") })
private List<Car> cars;

private List<Repair> repairs;

public Car() {
	super();
}

public Car(Long carId, String name, String description) {
	super();
	this.carId = carId;
	this.name = name;
	this.description = description;
}

public Long getCarId() {
	return carId;
}

public void setCarId(Long carId) {
	this.carId = carId;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public List<Account> getAccount() {
	return accounts;
}

public void setAccountId(List<Account> accounts) {
	this.accounts = accounts;
}


public List<Car> getCar() {
	return cars;
}

public List<Repair> getRepairs() {
	return repairs;
}

}
