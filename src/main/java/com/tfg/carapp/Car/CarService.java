package com.tfg.carapp.Car;

import java.util.List;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tfg.carapp.Repair.*;
import com.tfg.carapp.UserAccount.*;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CarService {

	@Autowired
	CarRepository carRepository;

	@Autowired
	AccountRepository accountRepository;
	
	@Autowired
	RepairRepository repairRepository;
	
	
	@Transactional
	public Boolean create(Car car, String email, Account account) throws OperationsException {
			Account a = accountRepository.findOneByEmail(email);
			Car c = carRepository.findOneById(car.getCarId());
			List<Account> accounts = car.getAccount();
			if (a== null && c==null) {
				accountRepository.save(account);
				carRepository.save(car);
				return Boolean.TRUE;
			}else {
				return Boolean.FALSE;
				//Comprobar si necesita una reparación
			}
	}

	@Transactional
	public List<Car> findAll() {
		return carRepository.findAll();
	}
 
	public List<Car> getCar(Long carId) {
		Car c = carRepository.findOneById(carId);
		if (c != null) {
			List<Car> list = c.getCar();
			return list;
		}
		return null;
	}

}


