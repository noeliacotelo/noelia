package com.tfg.carapp.Car;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {
		
	Car findOneByName(String name);
	Car findOneById(Long carId);
	

	@Query("select count(c) > 0 from Repair c where c.name = :name")
		boolean exists(@Param("name") String name);

}
