package com.tfg.carapp.Car;

import javax.management.OperationsException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
@Controller
public class CarController {
	
	@Autowired
	CarService carService;

	@PostMapping("createcar")
	@ResponseBody
	public Boolean create() throws OperationsException {
		return carService.create(null, null, null);
	}

}
