package com.tfg.carapp.UserAccount;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.transaction.annotation.Transactional;


import org.springframework.stereotype.Service;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Service
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;
	
	// Add an user to the Data Base
	@Transactional
	public Boolean save(Account account) {
		Account a = accountRepository.findOneByEmail(account.getEmail());
		if (a == null) {
			accountRepository.save(account);
			return Boolean.TRUE;
		} else
			return Boolean.FALSE;
	}

	// Delete an user from the Data Base
	@Transactional
	public void delete(Account account) {
		accountRepository.delete(account);
	}
	
	

}

