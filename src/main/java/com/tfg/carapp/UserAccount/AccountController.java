package com.tfg.carapp.UserAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AccountController {

	@Autowired
	private AccountService accountService;

	@PostMapping("signup")
	@ResponseBody
	Boolean signup(@RequestBody Account account) {
		Boolean a = accountService.save(account);
		return a;
	}
}
