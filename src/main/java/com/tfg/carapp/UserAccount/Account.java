package com.tfg.carapp.UserAccount;

import javax.persistence.*;
import java.util.*;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "Account")
@JsonIdentityInfo(
		  generator = ObjectIdGenerators.PropertyGenerator.class,
		  property = "accountId")

public class Account implements java.io.Serializable {

	private static final long serialVersionUID = -2943219370334852774L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "accountId")
	private Long accountId;
	
	private String nickname;

	@Column(unique = true)
	private String email;
	
	private String role = "ROLE_USER";
	
	private Instant created;

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Instant getCreated() {
		return created;
	}

	public void setCreated(Instant created) {
		this.created = created;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
